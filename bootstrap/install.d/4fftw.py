# -*- coding: utf-8 -*-
"""
Created on Tue May  5 18:48:01 2015

@author: gian
"""

import sys
import os


def install(rootpath,tmppath):
    src = "fftw-3.3.6-pl1.tar.gz"
    lnk = "http://www.fftw.org/%s" % (src)
    os.chdir(tmppath)
    os.system("wget %s" % (lnk))
    os.system("tar xzf  %s" % (src))
    os.chdir("fftw-3.3.6-pl1")
    os.system("./configure --prefix=%s --libdir=%s/lib --enable-float --enable-shared; make; make install;make clean" % (rootpath,rootpath))
    os.system("./configure --prefix=%s --libdir=%s/lib --enable-shared ;make; make install;make clean" % (rootpath,rootpath))
    os.system("./configure --prefix=%s --libdir=%s/lib --enable-long-double --enable-shared;make; make install;make clean" % (rootpath,rootpath))
    
    
def clean(rootpath,tmppath):
    os.chdir(tmppath)
    os.system("rm -rf fftw-3.3.6-pl1")    
 

if __name__ == "__main__":   
    install(os.environ.get("ENV_ROOT"),os.environ.get("ENV_TMP"))
    sys.exit(0)
        
        

