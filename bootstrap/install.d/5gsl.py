# -*- coding: utf-8 -*-
"""
Created on Tue May  5 18:48:01 2015

@author: gian
"""

import sys
import os


def install(rootpath,tmppath):
    src = "gsl-2.7.1.tar.gz"
    lnk = "ftp://ftp.gnu.org/gnu/gsl/%s" % (src)
    os.chdir(tmppath)
    os.system("wget %s" % (lnk))
    os.system("tar xzf  %s" % (src))
    os.chdir("gsl-2.7.1")
    os.system("./configure --prefix=%s" % (rootpath))
    os.system("make -j7")
    os.system("make install")
    
    
def clean(rootpath,tmppath):
    os.chdir(tmppath)
    os.system("rm -rf gsl-2.7.1")    
 

if __name__ == "__main__":   
    install(os.environ.get("ENV_ROOT"),os.environ.get("ENV_TMP"))
    sys.exit(0)
        
        

