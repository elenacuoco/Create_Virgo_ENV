# -*- coding: utf-8 -*-
"""
Created on Tue May  5 18:48:01 2015

"""

import sys
import os

sys.path += ["/usr/bin"]

def install(rootpath,tmppath):
    lnk = "https://git.ligo.org/virgo/virgoapp/Fr.git"
    os.chdir(tmppath)
    os.system("git clone %s" % (lnk))
    os.chdir("Fr")
    os.system("cmake -DCMAKE_INSTALL_PREFIX:PATH=$ENV_ROOT CMakeLists.txt")
    os.system("make")
    os.system("make  install")
    
    
    
    
def clean(rootpath,tmppath):
    os.chdir(tmppath)
    os.system("rm -rf Fr")    
 

if __name__ == "__main__":   
    install(os.environ.get("ENV_ROOT"),os.environ.get("ENV_TMP"))
    sys.exit(0)
        
        

