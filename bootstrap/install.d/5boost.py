# -*- coding: utf-8 -*-
"""
Created on Tue May  5 18:48:01 2015

@author: gian
"""

import sys
import os


def install(rootpath,tmppath):
    src = "boost_1_75_0.tar.bz2"
    lnk = "http://sourceforge.net/projects/boost/files/%s" % (src)
    
    os.chdir(rootpath)
    os.system("wget %s" % (lnk))
    os.system("tar -jxf  %s" % (src))
    os.chdir("boost_1_75_0")
    os.system("./bootstrap.sh --prefix=%s --with-libraries=serialization,python,signals,container,exception,thread" % (rootpath))
    os.system("./b2 stage threading=multi link=shared -j4 --prefix=%s" % (rootpath))
    os.system("cp bjam %s/bin" % (rootpath))
    os.chdir("tools/build")
    os.system("./bootstrap.sh")
    os.system("./b2 install --prefix=%s" % (rootpath))
    os.chdir(rootpath)
    os.system("rm boost_1_75_0.tar.bz2")
    os.chdir(rootpath)
    os.chdir("boost_1_75_0")
    os.system("cp -fr boost/ %s/include" % (rootpath))

def clean(rootpath,tmppath):
    os.chdir(rootpath)
    os.system("rm -rf boost_1_75_0")    
 

if __name__ == "__main__":   
    install(os.environ.get("ENV_ROOT"),os.environ.get("ENV_TMP"))
    sys.exit(0)
        
