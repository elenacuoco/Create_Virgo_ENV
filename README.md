## Create Virgo Environment 
Package to create a python virtual environment plus some library needed 
to compile p4TSA C++ library and python package and pyTSA.

------
### Installation
* Clone the repo
* Go in the directory bootstrap
* define 2 environment variables: ENV_ROOT (where you want to install the environment)
 and  ENV_TMP (temporary dir to install packages)
* to setup a python 3.* environment, launch the bash script ./startup_env3.*
* add the new environment to you 
    - bash shell: source ~/${ENV_ROOT}/environment
    - tcsh shell: source ~/${ENV_ROOT}/environment.csh
    